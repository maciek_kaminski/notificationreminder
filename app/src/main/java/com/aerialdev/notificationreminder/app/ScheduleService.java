package com.aerialdev.notificationreminder.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class ScheduleService extends Service {


    public ScheduleService() {
        super();
    }

    /**
     * Class for clients to access
     */
    public class ServiceBinder extends Binder {
        ScheduleService getService() {
            return ScheduleService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d("ScheduleService", "onStartCommand >>>> ScheduleService Received start id " + startId + ": " + intent + "    : Thread"+Thread.currentThread().getName());

        String action = intent.getStringExtra("action");
        if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
            rescheduleNotifications();
        }
        return START_NOT_STICKY;
    }

    private void rescheduleNotifications() {
        ApplicationDataHolder applicationContext = (ApplicationDataHolder) getApplicationContext();
        if(applicationContext!=null) {
            ArrayList<Notification> notificationArray = applicationContext.getNotificationArray();
            for(Notification notification:notificationArray){
                // TODO : change fire date!!!!
                Calendar cal = Calendar.getInstance();
                Calendar firstSchedule = notification.getFirstSchedule();
                setAlarm(notification);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Log.d("ScheduleService", "onBind >>>> ScheduleService ON BIND : Thread "+Thread.currentThread().getName());
        return mBinder;
    }

    // This is the object that receives interactions from clients. See
    private final IBinder mBinder = new ServiceBinder();

    /**
     * Show an alarm for a certain date when the alarm is called it will pop up a notification
     */
    public void setAlarm(Notification notification) {
        // This starts a new thread to set the alarm
        // You want to push off your tasks onto a new thread to free up the UI to carry on responding
        new AlarmTask(this, notification).run();
    }


    public void cancelAllNotifications() {
        AlarmManager am = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        ApplicationDataHolder applicationContext = (ApplicationDataHolder) getApplicationContext();
        if(applicationContext!=null) {
            ArrayList<Notification> notificationArray = applicationContext.getNotificationArray();
            Iterator<Notification> iterator = notificationArray.iterator();
            while(iterator.hasNext()){
                Notification not = iterator.next();
                NotificationIntent intent = new NotificationIntent(this, NotifyService.class);
                //intent.setNotificationId(666);
                intent.setType(String.valueOf(not.getAlarmManagerId()));
                PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, 0);
                am.cancel(pendingIntent);
                iterator.remove();
            }
            //SAVE State to DB
            applicationContext.syncWithDB();
        }
    }

    public boolean cancelNotification(Notification notification) {
        ApplicationDataHolder applicationContext = (ApplicationDataHolder) getApplicationContext();
        if(applicationContext!=null) {
            if(applicationContext.removeFromArray(notification)) {
                NotificationIntent intent = new NotificationIntent(this, NotifyService.class);
                //intent.setNotificationId(666);
                intent.setType(String.valueOf(notification.getAlarmManagerId()));
                PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, 0);

                AlarmManager am = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                am.cancel(pendingIntent);

                return true;
            }
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Log.d("ScheduleService", "onDestroy >>>> ScheduleService ON DESTROY  : Thread "+Thread.currentThread().getName());
    }
}
