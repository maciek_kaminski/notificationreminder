package com.aerialdev.notificationreminder.app;

import android.app.Application;

import java.util.ArrayList;
import java.util.Iterator;

public class ApplicationDataHolder extends Application {

    private NotificationsDAO notDAO;

    private ArrayList<Notification> notifications;


    public ArrayList<Notification> getNotificationArray(){
        return this.notifications;
    }

    public void addNotificationToArray(Notification not){
        if(!this.notifications.contains(not)) {
            this.notifications.add(not);
            notDAO.open();
            notDAO.createNotification(not);
            notDAO.close();
        }
    }

    public boolean removeFromArray(Notification not){
        if(this.notifications.remove(not)){
            syncWithDB();
            return true;
        }
        return false;
    }

    public boolean removeFromArrayByAMIndex(Integer id){
        Iterator<Notification> iterator = this.notifications.iterator();
        while(iterator.hasNext()){
            Notification next = iterator.next();
            if(next.getAlarmManagerId()==id){
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public void syncWithDB(){
        notDAO.open();
        notDAO.saveNotifications(notifications);
        notDAO.close();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notDAO = new NotificationsDAO(this);
        notDAO.open();
        notifications = notDAO.getAllNotifications();
        notDAO.close();
    }


    public void onClose(){
        notDAO.open();
        syncWithDB();
        notDAO.close();
    }

    public int getNextCounter() {
        return notifications.size()+1;
    }
}
