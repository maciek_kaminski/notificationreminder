package com.aerialdev.notificationreminder.app;

import android.content.Context;
import android.content.Intent;


public class NotificationIntent extends Intent {
    private int notificationId;

    public NotificationIntent(Context context, Class<NotifyService> notifyServiceClass) {
        super(context,notifyServiceClass);
    }

//    @Override
//    public boolean filterEquals(Intent other) {
//        return other != null && other instanceof NotificationIntent && (((NotificationIntent) other).getNotificationId() == getNotificationId() || getNotificationId()==666 || ((NotificationIntent) other).getNotificationId()==666) && super.filterEquals(other);
//    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }
}
