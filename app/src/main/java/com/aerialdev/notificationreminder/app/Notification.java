package com.aerialdev.notificationreminder.app;


import java.util.Calendar;

public class Notification {
    private long id;
    private String topic;
    private String message;
    private String packageName;
    private int alarmManagerId;
    private Calendar firstSchedule;
    private long frequency;


    public Notification(String topic, String message, String packageName) {
        this.id = -1;
        this.topic = topic;
        this.message = message;
        this.packageName = packageName;
    }
    public Notification(String topic, String message, String packageName, Calendar firstSchedule, long frequency) {
        this(topic, message, packageName);
        this.firstSchedule = firstSchedule;
        this.frequency = frequency;
    }
    public Notification(String topic, String message, String packageName,int alarmManagerId) {
        this.id = -1;
        this.topic = topic;
        this.message = message;
        this.packageName = packageName;
        this.alarmManagerId = alarmManagerId;
    }
    public Notification(String topic, String message, String packageName,int alarmManagerId, Calendar firstSchedule, long frequency) {
        this(topic, message, packageName, alarmManagerId);
        this.firstSchedule = firstSchedule;
        this.frequency = frequency;
    }

    public Notification(long id, String topic, String message, String packageName, int alarmManagerId) {
        this.id = id;
        this.topic = topic;
        this.message = message;
        this.packageName = packageName;
        this.alarmManagerId = alarmManagerId;
    }

    public Notification() {
        id = -1;
    }

    public long getId() {
        return id;
    }

    public int getAlarmManagerId() {
        return alarmManagerId;
    }

    public void setAlarmManagerId(int alarmManagerId) {
        this.alarmManagerId = alarmManagerId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Calendar getFirstSchedule() {
        return firstSchedule;
    }

    public void setFirstSchedule(Calendar firstSchedule) {
        this.firstSchedule = firstSchedule;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        if (alarmManagerId != that.alarmManagerId) return false;
        if (frequency != that.frequency) return false;
        if (id != that.id) return false;
        if (!firstSchedule.equals(that.firstSchedule)) return false;
        if (!message.equals(that.message)) return false;
        if (!packageName.equals(that.packageName)) return false;
        if (!topic.equals(that.topic)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + topic.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + packageName.hashCode();
        result = 31 * result + alarmManagerId;
        result = 31 * result + firstSchedule.hashCode();
        result = 31 * result + (int) (frequency ^ (frequency >>> 32));
        return result;
    }
}
