package com.aerialdev.notificationreminder.app;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class NotificationListFragment extends android.support.v4.app.ListFragment {

    private NotificationListAdapter adapter;
    private List<Notification> notificationsList;
    private ScheduleClient scheduleClient;

    @Override
    public ListAdapter getListAdapter() {
        return super.getListAdapter();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDivider(null);
        getListView().setDividerHeight(0);

        setHasOptionsMenu(true);

        scheduleClient = new ScheduleClient(getActivity());
        scheduleClient.doBindService();

        notificationsList = new ArrayList<Notification>();
        adapter = new NotificationListAdapter(getActivity(), R.layout.notificationrowlayout, notificationsList);
        adapter.notifyDataSetChanged();
        setListAdapter(adapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(adapter!=null){
                notificationsList = getNotificationsList();
                adapter.repaint(notificationsList);
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }


    private List<Notification> getNotificationsList() {
        ArrayList<Notification> notificationArray = new ArrayList<Notification>();
        ApplicationDataHolder applicationContext = (ApplicationDataHolder) getActivity().getApplicationContext();
        if (applicationContext != null) {
            notificationArray = applicationContext.getNotificationArray();
        }
        return notificationArray;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(R.string.clearAll);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                scheduleClient.cancelAllNotifications();
                notificationsList = new ArrayList<Notification>();
                adapter.repaint(notificationsList);
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        // When our activity is stopped ensure we also stop the connection to the service
        // this stops us leaking our activity into the system *bad*
        if(scheduleClient != null)
            scheduleClient.doUnbindService();
        super.onStop();
    }


    class NotificationListAdapter extends ArrayAdapter<Notification> {

        Context context;
        List<Notification> notifications;

        public NotificationListAdapter(Context context, int textViewResourceId, List<Notification> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            notifications = objects;
        }

        public void repaint(List<Notification> objects){
            notifications = objects;
            clear();
            addAll(notifications);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.notificationrowlayout, parent, false);

            TextView topic = (TextView) rowView.findViewById(R.id.notificationRowTopic);
            TextView message = (TextView) rowView.findViewById(R.id.notificationRowMessage);
            //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            //textView.setText(values[position]);
            // change the icon for Windows and iPhone
//            String s = values[position];
//            if (s.startsWith("iPhone")) {
//                imageView.setImageResource(R.drawable.no);
//            } else {
//                imageView.setImageResource(R.drawable.ok);
//            }

            Button deleteButton = (Button)rowView.findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(new DeleteButtonListener(position));

            if(notifications!=null){
                Notification notification = notifications.get(position);
                topic.setText(notification.getTopic());
                message.setText(notification.getMessage());
            }

            return rowView;
        }
    }

    private class DeleteButtonListener implements View.OnClickListener {
        private int position;

        public DeleteButtonListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Notification notification = notificationsList.get(position);
            if(scheduleClient.cancelNotification(notification)){
                notificationsList = getNotificationsList();
                adapter.repaint(notificationsList);
            }else{
                Toast.makeText(getActivity().getBaseContext(), R.string.notDeleteError, Toast.LENGTH_SHORT);
            }
        }
    }
}
