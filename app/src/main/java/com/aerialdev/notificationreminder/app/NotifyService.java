package com.aerialdev.notificationreminder.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;

/**
 * This service is started when an Alarm has been raised
 *
 * We pop a notification into the status bar for the user to click on
 * When the user clicks the notification a new activity is opened
 *
 * @author paul.blundell
 */
public class NotifyService extends Service {

    /**
     * Class for clients to access
     */
    public class ServiceBinder extends Binder {
        NotifyService getService() {
            return NotifyService.this;
        }
    }

    // Unique id to identify the notification.
    private static final int NOTIFICATION = 666;
    // Name of an intent extra we can use to identify if this service was started to create a notification
    public static final String INTENT_NOTIFY = "com.aerialdev.notificationreminder.app.INTENT_NOTIFY";
    // The system notification manager
    private NotificationManager mNM;

    @Override
    public void onCreate() {
        //Log.i("NotifyService", "NotifyService onCreate() Thread: "+Thread.currentThread().getName());
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d("NotifyService", " NotifyService Received onStartCommand id " + startId + ": " + intent + "  Thread:"+Thread.currentThread().getName());

        // If this service was started by out AlarmTask intent then we want to show our notification
        if(intent.getBooleanExtra(INTENT_NOTIFY, false)) {
            showNotification(intent.getStringExtra("topic"), intent.getStringExtra("text"), intent.getStringExtra("apppackage"), intent.getIntExtra("notificationId", NOTIFICATION));
        }

        // We don't care if this service is stopped as we have already delivered our notification
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Log.d("NotifyService", " NotifyService ON BIND Thread:"+Thread.currentThread().getName());
        return mBinder;
    }

    // This is the object that receives interactions from clients
    private final IBinder mBinder = new ServiceBinder();

    /**
     * Creates a notification and shows it in the OS drag-down status bar
     * @param topic
     * @param text
     * @param apppackage
     */
    private void showNotification(String topic, String text, String apppackage, int notificationId) {
        // This is the icon to use on the notification
        int icon = android.R.drawable.ic_dialog_alert;
        // What time to show on the notification
        long time = System.currentTimeMillis();

        Notification notification = new Notification(icon, text, time);

        PackageManager manager = getPackageManager();
        Intent inte = null;
        PendingIntent contentIntent = null;
        if (manager != null) {
            inte = manager.getLaunchIntentForPackage(apppackage);
        }
        if (inte != null) {
            inte.addCategory(Intent.CATEGORY_LAUNCHER);
        }else{
            inte = StaticAppsDataHolder.getInstance().getIntentForPackage(apppackage);
        }

        if(inte!=null) {
            contentIntent = PendingIntent.getActivity(this, 0, inte, 0);
        }

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, topic, text, contentIntent);

        // Clear the notification when it is pressed
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;

            // Send the notification to the system.
        mNM.notify(notificationId, notification);
        //Log.d("NotifyService", " before stopSelf");
        // Stop the service when we are finished
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Log.d("NotifyService", "onDestroy >>>> NotifyService ON DESTROY  : Thread "+Thread.currentThread().getName());
    }
}