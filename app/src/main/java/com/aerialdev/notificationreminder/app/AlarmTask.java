package com.aerialdev.notificationreminder.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;

import java.util.Calendar;

/**
 * Set an alarm for the date passed into the constructor
 * When the alarm is raised it will start the NotifyService
 *
 * This uses the android build in alarm manager *NOTE* if the phone is turned off this alarm will be cancelled
 *
 * This will run on it's own thread.
 *
 * @author paul.blundell
 */
public class AlarmTask implements Runnable{
    // The date selected for the alarm
    private final Calendar date;
    // The android system alarm manager
    private final AlarmManager am;
    // Your context to retrieve the alarm manager from
    private final Context context;
    private final long interval;
    private Notification notification;
    private NotificationIntent intent;

    public AlarmTask(Context context,Notification notification) {
        this.context = context;
        this.notification = notification;
        this.am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        this.date = notification.getFirstSchedule();
        this.interval = notification.getFrequency();
    }

    @Override
    public void run() {
        //Log.d("AlarmTask", " Alarm Task -- in run() method : Thread "+Thread.currentThread().getName());
        // Request to start are service when the alarm date is upon us
        // We don't start an activity as we just want to pop up a notification into the system bar not a full activity

        PendingIntent pendingIntent = PendingIntent.getService(context, 0, notificationToIntent(notification), PendingIntent.FLAG_CANCEL_CURRENT);
        // Sets an alarm - note this alarm will be lost if the phone is turned off and on again
        am.setRepeating(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), interval, pendingIntent);


        //SAVE ADDED NOTIFICATION
        ApplicationDataHolder applicationContext = (ApplicationDataHolder) context.getApplicationContext();
        if (applicationContext != null) {
            applicationContext.addNotificationToArray(notification);
        }
    }

    private NotificationIntent notificationToIntent(Notification not){
        NotificationIntent intent = new NotificationIntent(context, NotifyService.class);
        intent.setNotificationId(not.getAlarmManagerId());
        intent.putExtra("notificationId", not.getAlarmManagerId());
        intent.putExtra(NotifyService.INTENT_NOTIFY, true);
        intent.putExtra("topic", not.getTopic());
        intent.putExtra("text", not.getMessage());
        intent.putExtra("apppackage", not.getPackageName());
        intent.setType(String.valueOf(not.getAlarmManagerId()));
        return intent;
    }
}