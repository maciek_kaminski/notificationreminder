package com.aerialdev.notificationreminder.app;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_NOTIFICATIONS = "notifications";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOTIFICATION_TOPIC = "topic";
    public static final String COLUMN_NOTIFICATION_MESSAGE = "notification";
    public static final String COLUMN_PACKAGE = "package";
    public static final String COLUMN_FIRST_SCHEDULE = "first_schedule";
    public static final String COLUMN_FREQUENCY = "frequency";

    private static final String DATABASE_NAME = "notifications.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NOTIFICATIONS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_NOTIFICATION_TOPIC
            + " text, " + COLUMN_NOTIFICATION_MESSAGE
            + " text, " + COLUMN_PACKAGE
            + " integer, " + COLUMN_FIRST_SCHEDULE
            + " integer, " + COLUMN_FREQUENCY
            + " text );";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATIONS);
        onCreate(db);
    }

}