package com.aerialdev.notificationreminder.app;

import android.app.Dialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;


public class MainFragment extends Fragment {

    private View view;
    private LayoutInflater inflater;
    // This is a handle so that we can call methods on our service
    private ScheduleClient scheduleClient;
    // This is the date picker used to select the date for our notification
    private DatePicker picker;
    private TimePicker timePicker;
    private Spinner freqSpinner;
    private String selectedPackage;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);

        setHasOptionsMenu(false);

        this.inflater = inflater;
        // Create a new service client and bind our activity to this service
        scheduleClient = new ScheduleClient(getActivity());
        scheduleClient.doBindService();

        // Get a reference to our date picker
        picker = (DatePicker) view.findViewById(R.id.scheduleDatePicker);
        timePicker = (TimePicker) view.findViewById(R.id.scheduleTimePicker);
        timePicker.setIs24HourView(true);
        freqSpinner = (Spinner) view.findViewById(R.id.freqSpinner);
        selectedPackage = getString(R.string.default_pacakge);


        ((Button)view.findViewById(R.id.button_app)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectPackageButtonClicked(v);
            }
        });
        ((Button)view.findViewById(R.id.selectButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateSelectedButtonClick(v);
            }
        });

        return view;
    }


    public void onDateSelectedButtonClick(View v){
        // Get the date from our datepicker
        int day = picker.getDayOfMonth();
        int month = picker.getMonth();
        int year = picker.getYear();

        Integer currentHour = timePicker.getCurrentHour();
        Integer currentMinute = timePicker.getCurrentMinute();
        // Create a new calendar set to the date chosen
        // we set the time to midnight (i.e. the first minute of that day)
        Calendar c = Calendar.getInstance();
        //noinspection ResourceType
        c.set(year, month, day);
        c.set(Calendar.HOUR_OF_DAY, currentHour);
        c.set(Calendar.MINUTE, currentMinute);
        c.set(Calendar.SECOND, 0);

        Editable text = ((EditText) view.findViewById(R.id.edit_text)).getText();
        Editable topic = ((EditText) view.findViewById(R.id.edit_topic)).getText();
        long freq = getResources().getIntArray(R.array.integer_freq)[freqSpinner.getSelectedItemPosition()];

        ApplicationDataHolder applicationContext = (ApplicationDataHolder)  getActivity().getApplicationContext();
        if(applicationContext!=null) {

            // Ask our service to set an alarm for that date, this activity talks to the client that talks to the service
            Notification notification = new Notification(topic.toString(), text.toString(), selectedPackage, applicationContext.getNextCounter(), c, freq);
            scheduleClient.setAlarmForNotification(notification);

            // Notify the user what they just did
            Toast.makeText(getActivity(), "Notification set for: " + day + "/" + (month + 1) + "/" + year + " every " + freqSpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
        }
    }


    public void onSelectPackageButtonClicked(View v){
        final Dialog dlg = new Dialog(getActivity());
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogLayout = inflater.inflate(R.layout.activity_second, null, false);
        dlg.setContentView(dialogLayout);

        PackageManager pm = getActivity().getPackageManager();
        assert pm != null;
        final List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        assert dialogLayout != null;

        ListView appList = (ListView)dialogLayout.findViewById(R.id.list_apps);
        AppListAdapter adapter = new AppListAdapter(getActivity(), R.layout.app_element, packages);
        appList.setAdapter(adapter);

        appList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position>0) {
                    ((Button) MainFragment.this.view.findViewById(R.id.button_app)).setText(getActivity().getPackageManager().getApplicationLabel(packages.get(position)));
                    selectedPackage = packages.get(position).packageName;
                }else{
                    ((Button) MainFragment.this.view.findViewById(R.id.button_app)).setText(R.string.emptyApp);
                    selectedPackage = "";
                }
                dlg.hide();
            }
        });

        dlg.show();
    }


    @Override
    public void onStop() {
        // When our activity is stopped ensure we also stop the connection to the service
        // this stops us leaking our activity into the system *bad*
        if(scheduleClient != null)
            scheduleClient.doUnbindService();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

}
