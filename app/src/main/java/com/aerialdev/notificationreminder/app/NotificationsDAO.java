package com.aerialdev.notificationreminder.app;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;

public class NotificationsDAO { // Database fields
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;
    private String[] allColumns = {SQLiteHelper.COLUMN_ID,
            SQLiteHelper.COLUMN_NOTIFICATION_TOPIC, SQLiteHelper.COLUMN_NOTIFICATION_MESSAGE, SQLiteHelper.COLUMN_PACKAGE, SQLiteHelper.COLUMN_FIRST_SCHEDULE, SQLiteHelper.COLUMN_FREQUENCY};

    public NotificationsDAO(Context context) {
        dbHelper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createNotification(Notification notification) {
        long res;
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_NOTIFICATION_TOPIC, notification.getTopic());
        values.put(SQLiteHelper.COLUMN_NOTIFICATION_MESSAGE, notification.getMessage());
        values.put(SQLiteHelper.COLUMN_PACKAGE, notification.getPackageName());
        values.put(SQLiteHelper.COLUMN_FIRST_SCHEDULE, notification.getFirstSchedule().getTimeInMillis());
        values.put(SQLiteHelper.COLUMN_FREQUENCY, notification.getFrequency());
        res = database.insert(SQLiteHelper.TABLE_NOTIFICATIONS, null,
                values);
        Cursor cursor = database.query(SQLiteHelper.TABLE_NOTIFICATIONS,
                allColumns, SQLiteHelper.COLUMN_ID + " = " + res, null,
                null, null, null);
        if (cursor.getCount() != 1) {
            return -1;
        }
        notification.setId(res);
        return res;
    }

    public void deleteNotification(long id) {
        System.out.println("Comment notification with id: " + id);
        database.delete(SQLiteHelper.TABLE_NOTIFICATIONS, SQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public ArrayList<Notification> getAllNotifications() {
        ArrayList<Notification> notifications = new ArrayList<Notification>();

        Cursor cursor = database.query(SQLiteHelper.TABLE_NOTIFICATIONS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Notification notification = cursorToNotification(cursor);
            notifications.add(notification);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return notifications;
    }

    private Notification cursorToNotification(Cursor cursor) {
        Notification notification = new Notification();
        notification.setId(cursor.getLong(0));
        notification.setTopic(cursor.getString(1));
        notification.setMessage(cursor.getString(2));
        notification.setPackageName(cursor.getString(3));
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(cursor.getLong(4));
        notification.setFirstSchedule(instance);
        notification.setFrequency(cursor.getLong(5));
        return notification;
    }

    public void saveNotifications(ArrayList<Notification> notifications) {
        database.delete(SQLiteHelper.TABLE_NOTIFICATIONS, null, null);
        for (Notification not : notifications) {
            long notification = createNotification(not);
            not.setId(notification);
        }
        int shouldBeSizeofArray = getSize();
        if (shouldBeSizeofArray != notifications.size()) {
            throw new RuntimeException("Not all Notofications saved");
        }
    }

    private int getSize() {
        Cursor cursor = database.query(SQLiteHelper.TABLE_NOTIFICATIONS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}