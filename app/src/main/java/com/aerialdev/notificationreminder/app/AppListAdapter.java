package com.aerialdev.notificationreminder.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class AppListAdapter extends ArrayAdapter<ApplicationInfo> {

    private List<ApplicationInfo> data;

    public AppListAdapter(Context context, int textViewResourceId, List<ApplicationInfo> data) {
        super(context, textViewResourceId, data);
        this.data = data;
        this.data.add(0, new ApplicationInfo());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.app_element, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.app_package);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.app_icon);
        try {
            //Resources resourcesForApplication = getContext().getPackageManager().getResourcesForApplication(data.get(position).getPackageName());
//            Drawable drawable = resourcesForApplication.getDrawable(data.get(position).getIconId());
//            if(drawable!=null){
//                imageView.setImageDrawable(drawable);
//            }
            //textView.setText(resourcesForApplication.getString(data.get(position).getNameId()));
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
            if(position>0) {
                imageView.setImageDrawable(getContext().getPackageManager().getApplicationIcon(data.get(position)));
                textView.setText(getContext().getPackageManager().getApplicationLabel(data.get(position)));
            }else{
                textView.setText(R.string.emptyApp);
            }
        }catch (Resources.NotFoundException e){
            e.printStackTrace();
        }

        return convertView;
    }

}
