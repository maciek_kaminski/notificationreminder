package com.aerialdev.notificationreminder.app;

import android.content.Intent;

import java.util.HashMap;
import java.util.Map;

public class StaticAppsDataHolder {
    private static StaticAppsDataHolder mInstance = null;


    private Map<String, Intent> mapOfIntents;


    private StaticAppsDataHolder(){
        mapOfIntents = new HashMap<String, Intent>();
        initializeMap(mapOfIntents);
    }

    private void initializeMap(Map<String, Intent> mapOfIntents) {
        mapOfIntents.put("com.android.phone", new Intent(Intent.ACTION_DIAL, null));
    }


    public static StaticAppsDataHolder getInstance(){
        if(mInstance == null)
        {
            mInstance = new StaticAppsDataHolder();
        }
        return mInstance;
    }


    public Intent getIntentForPackage(String packageName){
        return mapOfIntents.get(packageName);
    }
}
