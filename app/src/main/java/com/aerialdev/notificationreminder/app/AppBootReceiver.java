package com.aerialdev.notificationreminder.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class AppBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, ScheduleService.class);
        service.putExtra("action", Intent.ACTION_BOOT_COMPLETED);
        context.startService(service);
    }
}
